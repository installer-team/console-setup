===========
WHAT IS IT?
===========

This package provides the console with the same keyboard configuration
scheme that X Window System has. In result, there is no need to
duplicate or change the console keyboard files just to make simple
customizations such as the use of dead keys, the key functioning as
AltGr or Compose key, the key(s) to switch between Latin and non-Latin
layouts, etc.  Besides the keyboard, the package configures also the
font on the console.  It includes a rich collection of fonts and
supports several languages that would be otherwise unsupported on the
console (such as Armenian, Georgian, Lao and Thai).

The package supports
PC, Amiga, Atari, old Macintosh, Sun4 and Sun5 keyboards on Linux.

There are no binaries in the package: it can be compiled on one
platform and used on another.  For its compilation it requires GNUmake
and Perl.  

The package can be installed in two ways -- with or without
precompiled keyboard files.  If it is installed with precompiled
keyboard files, then it won't require Perl for its normal work, but
not all possible keyboard configurations will be properly supported.
If the package is installed without precompiled keyboard files, then
it will require Perl for its normal work but you will be able to use
arbitrary keyboard configuration on the console.


============
INSTALLATION
============

First you need to compile the package.  Notice, that this can take a
long time, especially if you will use precompiled keyboard files.  Use
one of the following commands:

   make build-linux         # For Linux without precompiled keyboard files
   make build-mini-linux    # For Linux with precompiled keyboard files

Then in order to install the package in /usr/local use one of the
following commands:

   make install-linux
   make install-mini-linux

You don't have to install the package in /usr/local -- you can install
it anywhere and test how it works from there, for example:

   mkdir /tmp/cs
   make prefix=/tmp/cs install-linux 

The Makefile also provides uninstall targets (such as uninstall-linux)
but if you have installed 'stow', then it is recommended to use it for
the deinstallation.

In order to use console-setup it is not necessary to have installed X
Window because the package comes with its own version of the X
keyboard files.  By default they are installed in
/usr/local/etc/console-setup/ckb.  If, however, X Window is installed,
then you can remove this directory and console-setup will use the same
files X uses.

If X Window is installed in the system and you want to use
console-setup with precompiled keyboard files, then compile it with

   make xkbdir=/usr/local/share/X11/xkb build-mini-linux


===========
BASIC USAGE
===========

First look at the configuration files keyboard and console-setup.  By
default they will be installed in /usr/local/etc/default.  Read their
manual pages: keyboard(5) and console-setup(5).  You can find
preformatted text version of these manual pages in the directory man/
of the source package.

When you are ready, run the command 'setupcon' without parameters and
that's it.  In order to make your system automatically configure the
console, simply put the command 'setupcon' in some of the boot
scripts.

-- 
Copyright (C) 2011 Anton Zinoviev <anton@lml.bas.bg>

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
